# Fedora Docs "HowTo" for sporadic contributors

## 7 clicks to put forward your suggested Docs change/update without preceding knowledge of git or merge requests

### There is no risk of breaking anything :) ... and all will be done in a GUI within your browser!

If you have found something on a Docs page, which you want to add/remove/change/update, you can see three buttons on the top left, which can be used to directly put forward your suggestion without knowledge of git or the structures with which the Docs pages are organized (repository structures). The buttons will automatically forward you correspondingly.

Left of the buttons, you see the Fedora release (e.g., "F36") to which the Docs page you have opened relates to. Docs pages relate to a specific Fedora release, although changes/updates can apply to several releases' Docs pages. If the latter is the case, just choose any version that needs the change/update. If you do not know that, ignore it and just go ahead. The left button forwards to the history of the opened Docs page and is <b>not</b> relevant for you. The middle button forwards to "edit the page" that is currently opened, which is what you have to click to put forward your suggested change/update. The right button forwards to "report an issue" if you only want to make us aware of some need to change/update without making this yourself. Click the middle ("edit the page") button to go ahead with putting forward your suggestions:

<p align="center"><img src="1CUT-changeIdentified.png"
 alt="Figure 1" title="1" align="center" /></p>
<p align="center"><img src="1A-changeIdentified.png"
 alt="Figure 1" title="1" align="center" /></p>

Now, log in to your GitLab account, or create one if you do not have one yet (<b>see the image below</b>). <b>Alternatively</b>, you can also use the Fedora Account System to log into GitLab, but you have to do this in advance before clicking the "edit the page" button with the following link: [https://gitlab.com/groups/fedora/-/saml/sso](https://gitlab.com/groups/fedora/-/saml/sso)

<p align="center"><img src="2-GitLabSignin.png"
 alt="Figure 1" title="1" align="center" /></p>

Click now on the blue button "Open in Web IDE":

<p align="center"><img src="3-EditPage.png"
 alt="Figure 1" title="1" align="center" /></p>

Now, click on the button "fork" that has just appeared:

<p align="center"><img src="4-EditFork.png"
 alt="Figure 1" title="1" align="center" /></p>

Then, search the change/update within the Docs page you have now opened in the Web IDE and ...

<p align="center"><img src="5-webIDE.png"
 alt="Figure 1" title="1" align="center" /></p>

... make the change/update you want to suggest:

<p align="center"><img src="6-EditInWebIDE.png"
 alt="Figure 1" title="1" align="center" /></p>

As soon as any change has been made, the button "Create commit..." will turn blue (see the image below). As soon as you have made all changes/updates you want to suggest, click the blue "Create commit..." button:

<p align="center"><img src="7-DoneInWebIDE.png"
 alt="Figure 1" title="1" align="center" /></p>

Now, you will see the differences between the original version and your suggested version so that you can do a review:

<p align="center"><img src="8-CheckChange.png"
 alt="Figure 1" title="1" align="center" /></p>

After review, add a "Commit Message" briefly explaining what your suggestion is about. Then, click the blue "Commit" button:

<p align="center"><img src="9-CommitMessage.png"
 alt="Figure 1" title="1" align="center" /></p>

Now you are at the final page to finalize your Merge Request, which contains a default Title and default Description that both are likely to need adjustment:

<p align="center"><img src="A-StartMergeRequest.png"
 alt="Figure 1" title="1" align="center" /></p>

Now, you should adjust the Title and the Description of the page according to your suggestion (feel free to copy-paste the Commit Message) and then, scroll down to the blue button "Create merge request" and click it:

<p align="center"><img src="B-EditTitleAndCreateMR.png"
 alt="Figure 1" title="1" align="center" /></p>

Generally, you are done now! You can ignore any issues about "branches". But feel free to ...

<p align="center"><img src="C-MR.png"
 alt="Figure 1" title="1" align="center" /></p>

 ... add comments and discuss your suggestions with the Fedora Docs team. You can return to this page later as well. Also, at this point, feel free to note in a comment if you already know which Fedora releases are affected, but this is not obligatory:

<p align="center"><img src="D-CommentsInMR.png"
 alt="Figure 1" title="1" align="center" /></p>

<p align="center"><img src="E-MRwithComments.png"
 alt="Figure 1" title="1" align="center" /></p>

Thanks for your contribution!
